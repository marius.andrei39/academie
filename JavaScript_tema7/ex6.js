//6.1
const spaceShuttle="Determination";
let shuttleSpeed=17500; //mph
let distanceMars=225000000; //km
let distanceMoon=384400; //km
const mpkm=0.621; //miles per kilometer

//6.2
let distanceMarsMiles=distanceMars*mpkm;
let hoursToMars=distanceMarsMiles/shuttleSpeed;
let daysToMars=(hoursToMars/24).toFixed(2);

//6.3
alert(`${spaceShuttle} will take ${daysToMars} days to reach Mars.`);

//6.4
let distanceMoonMiles=distanceMoon*mpkm;
let hoursToMoon=distanceMoonMiles/shuttleSpeed;
let daysToMoon=(hoursToMoon/24).toFixed(2);
alert(`${spaceShuttle} will take ${daysToMoon} days to reach Moon.`);