const selectBox = document.querySelector(".select-box"),
selectBtnX = selectBox.querySelector(".options .playerX"),
changeTitle = selectBox.querySelector(".title"),
selectBtnO = selectBox.querySelector(".options .playerO"),
selectSingle = selectBox.querySelector(".options .single"),
selectMulti = selectBox.querySelector(".options .multi"),
selectChoose = selectBox.querySelector(".options .chooseMode"),
selectChoose2 = selectBox.querySelector(".options .chooseSingle"),
selectChoose3 = selectBox.querySelector(".options .chooseMulti"),
playBoard = document.querySelector(".play-board"),
players = document.querySelector(".players"),
allBox = document.querySelectorAll("section span"),
resultBox = document.querySelector(".result-box"),
wonText = resultBox.querySelector(".won-text"),
replayBtn = resultBox.querySelector("button"),
menuBtn = document.getElementById("menu"),
resetBtn = document.getElementById("reset"),
player1Turn = document.getElementById("playerx"),
player2Turn = document.getElementById("player0"),
playMulti = selectBox.querySelector(".startMulti"),
radioX = document.getElementById("Xplayer"),
radioO = document.getElementById("Oplayer");
let playerXIcon = "fas fa-times", //X icon
playerOIcon = "far fa-circle", //O icon
moves = 0, //used to count how many moves the players did
playerSign = "X", //by default the player is X
runBot = true, //used for single player's bot
botSign = null, //bot's sign
multiplayer = false; //used when player chooses to play multiplayer



// startup function
window.onload = ()=>{
    for (let i = 0; i < allBox.length; i++) {
       allBox[i].setAttribute("onclick", "clickedBox(this)");
    }
}


//Single player function
selectSingle.onclick = ()=>{
    selectChoose.classList.add("hide"); //initial page dissapear
    selectChoose2.classList.add("show"); //single player options appear
    changeTitle.innerText = "Which player would you like to be?"; //change title
}

selectSingle.addEventListener('mouseover', function(){ //makes the single player image appear
    document.querySelector(".player-type").style.cssText =`opacity: 1;
    transition: all 0.5s ease;`;
})

selectSingle.addEventListener('mouseout', function(){ //makes the single player image dissappear
    document.querySelector(".player-type").style.cssText=`opacity: 0;
    transition: all 0.5s ease;`;
})

selectMulti.addEventListener('mouseover', function(){ //makes the multiplayer image appear
    document.querySelector(".player-type2").style.cssText =`opacity: 1;
    transition: all 0.5s ease;`;
})

selectMulti.addEventListener('mouseout', function(){ //makes the multiplayer image dissappear
    document.querySelector(".player-type2").style.cssText=`opacity: 0;
    transition: all 0.5s ease;`;
})

//Multiplayer function
selectMulti.onclick = ()=>{
    selectChoose.classList.add("hide"); //initial page dissapear
    selectChoose3.classList.add("show"); //multiplayer options appear
    changeTitle.innerText = "Enter player names"; //change title
}

//Single player choose X
selectBtnX.onclick = ()=>{
    selectBox.classList.add("hide"); //menu dissapears
    playBoard.classList.add("show"); //board appears
    botSign = "O";
}

//Single player choose O
selectBtnO.onclick = ()=>{ 
    botSign = "X";
    selectBox.classList.add("hide");
    playBoard.classList.add("show");
    players.setAttribute("class", "players active player"); //used for top player display turn; when active it highlights players turn
    players.classList.remove("active");
    let randomTimeDelay = ((Math.random() * 1000) + 500).toFixed(); //bot's response is delayed randomly
    setTimeout(()=>{
        bot(runBot);
    }, randomTimeDelay);
}

playMulti.onclick = ()=>{
    let player1 = document.getElementById("playa1").value; //get player 1 name from input
    let player2 = document.getElementById("playa2").value; //get player 2 name from input
    if(radioO.checked){ //does the name change when O radio is checked so X can always start
        let univPlayer = player1;
        player1 = player2;
        player2 = univPlayer;
    }
    if(player1!==''&&player2!==''){
    selectBox.classList.add("hide");
    playBoard.classList.add("show");
    player1Turn.innerHTML=`${player1}'s turn (X)`; //change name on board display
    player2Turn.innerHTML=`${player2}'s turn(O)`;
    multiplayer = true;
    botSign='O';
    }else{
    changeTitle.innerText = "Names can't be blank!!";
    changeTitle.style.color='red';
    }

}

function clickedBox(element){
    if(multiplayer){
        let player1 = document.getElementById("playa1").value;
        let player2 = document.getElementById("playa2").value;
        if(players.classList.contains("player")){ //player 1 will always be X so we choose player 2 to be O
            playerSign = player2; //used to assign player win name
            element.innerHTML = `<i class="${playerOIcon}"></i>`;
            moves=moves+1;
            players.classList.remove("active");
            players.classList.remove("player");
            element.setAttribute("id", playerSign); //keep win condition 
        }else{
            playerSign = player1;
            element.innerHTML = `<i class="${playerXIcon}"></i>`; 
            element.setAttribute("id", playerSign);
            players.classList.add("active");
            players.setAttribute("class", "players active player");
            moves=moves+1;
        }
        selectWinner();
        element.style.pointerEvents = "none";
    }else{
        if(players.classList.contains("player")){ //by default player is X, we add the player class when we choose O
            playerSign = "O";
            element.innerHTML = `<i class="${playerOIcon}"></i>`;
            players.classList.remove("active");
            element.setAttribute("id", playerSign);
            moves=moves+1;
        }else{
            element.innerHTML = `<i class="${playerXIcon}"></i>`;
            element.setAttribute("id", playerSign);
            players.classList.add("active");
            moves=moves+1;
        }
        selectWinner();
        element.style.pointerEvents = "none"; //can't do any clicks while bot does a move
        playBoard.style.pointerEvents = "none";
        let randomTimeDelay = ((Math.random() * 1000) + 200).toFixed();
        setTimeout(()=>{
            bot(runBot);
        }, randomTimeDelay);
    }
    
}

function bot(){//bot used in single player
    let array = [];
    if(runBot){
        playerSign = "O";
        for (let i = 0; i < allBox.length; i++) {
            if(allBox[i].childElementCount == 0){
                array.push(i);
            }
        }
        let randomBox = array[Math.floor(Math.random() * array.length)]; //bot chooses a random box, no predictive moves
        if(array.length > 0){ //while there are still moves asign bot's sign
            if(botSign==="O"){
            allBox[randomBox].innerHTML = `<i class="${playerOIcon}"></i>`;
            allBox[randomBox].setAttribute("id", botSign);
            players.classList.remove("active");
            moves=moves+1;
        }
        else{
            allBox[randomBox].innerHTML = `<i class="${playerXIcon}"></i>`;
            allBox[randomBox].setAttribute("id", botSign);
            players.classList.add("active");
            moves=moves+1;
        }
            selectWinner();
        }
        allBox[randomBox].style.pointerEvents = "none";
        playBoard.style.pointerEvents = "auto";
        playerSign = "X";
    }
}

function getIdVal(classname){ //gets element from checked boxes
    return document.querySelector(".box" + classname).id;
}

function checkIdSign(val1, val2, val3, sign){ //checks if 3 signs are alike, used in win conditioning
    if(getIdVal(val1) == sign && getIdVal(val2) == sign && getIdVal(val3) == sign){
        return true;
    }
}

function resetTable(){
    for (let i = 0; i < allBox.length; i++) {
        allBox[i].setAttribute("id", null);
        allBox[i].innerHTML = "";
        allBox[i].style.pointerEvents = "auto";
        moves=0;
     }
}

function selectWinner(){//checks winning conditions
    //draw condition
    if(getIdVal(1) != "" && getIdVal(2) != "" && getIdVal(3) != "" && getIdVal(4) != "" && getIdVal(5) != "" && getIdVal(6) != "" && getIdVal(7) != "" && getIdVal(8) != "" && getIdVal(9) != "" && moves===9){
        runBot = false;
        bot(runBot);
        setTimeout(()=>{
            resultBox.classList.add("show");
            playBoard.classList.remove("show");
        }, 700);
        wonText.textContent = "Match has been drawn!";
    }
    //bot win condition
    if(checkIdSign(1,2,3,botSign) || checkIdSign(4,5,6, botSign) || checkIdSign(7,8,9, botSign) || checkIdSign(1,4,7, botSign) || checkIdSign(2,5,8, botSign) || checkIdSign(3,6,9, botSign) || checkIdSign(1,5,9, botSign) || checkIdSign(3,5,7, botSign)){
        runBot = false;
        bot(runBot);
        setTimeout(()=>{
            resultBox.classList.add("show");
            playBoard.classList.remove("show");
        }, 700);
        wonText.innerHTML = `Player <p>${botSign}</p> won the game!`;
    }
    //player win condition
    if(checkIdSign(1,2,3,playerSign) || checkIdSign(4,5,6, playerSign) || checkIdSign(7,8,9, playerSign) || checkIdSign(1,4,7, playerSign) || checkIdSign(2,5,8, playerSign) || checkIdSign(3,6,9, playerSign) || checkIdSign(1,5,9, playerSign) || checkIdSign(3,5,7, playerSign)){
        runBot = false;
        bot(runBot);
        setTimeout(()=>{
            resultBox.classList.add("show");
            playBoard.classList.remove("show");
        }, 700);
        wonText.innerHTML = `Player <p>${playerSign}</p> won the game!`;
    }

}

//goes back to the menu/refreshes game
replayBtn.onclick = ()=>{
    window.location.reload();
}

menuBtn.onclick = ()=>{
    window.location.reload();
}

//resets the table
resetBtn.onclick = ()=>{
    resetTable();
}
