class validatorError extends Error{
    constructor(message){
        super(message);
        this.name="validatorError"
    }

    validare(object){
        if(!object.age){
            throw new validatorError("Age field not there / Introdu varsta pls");
        }else if(!object.name){
            throw new validatorError("Name field not there / Introdu numele pls");
        }
        if(typeof object.name !=="string"){
            throw new validatorError("Introdu un nume valid");
        }
        if(typeof object.age !=="number"){
            throw new validatorError("Varsta nu este numar");
        }
    }
}


function validateJson(json){

    try{
        let user=JSON.parse(json);

       let validarea=new validatorError("");
       validarea.validare(user); 

        alert(`${user.name} a implinit frumoasa varsta de ${user.age} de ani!`);
        return user;

    }catch(error){
        alert("JSON Error: "+ error.message);
    }finally{
        alert('Happy Bday!');
    }
}


