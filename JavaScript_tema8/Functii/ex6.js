function squareNumber(number){
    square=number*number;
    console.log(`The resulf of squaring the number ${number} is ${square}.`);
    return square;
}

squareNumber(3);

function halfNumber(number){
    half=number/2;
    console.log(`Half of ${number} is ${half}`);
    return half;
}

halfNumber(5);

function percentOf(number1, number2){
    percent=100*number1/number2;
    console.log(`${number1} is ${percent}% of ${number2}`);
    return percent;
}

percentOf(3,4);

function areaOfCircle(radius){
    area=Math.PI*Math.pow(radius,2);
    console.log(`The area for a circle with radius ${radius} is ${area.toFixed(2)}.`);
    return area;
}

areaOfCircle(5);


function takeANumber(number){
    half=halfNumber(number);
    square=squareNumber(half);
    area=areaOfCircle(square).toFixed(2);
    percent=percentOf(area,square).toFixed(2);
    console.log(`The number is ${number}, half of it is ${half}, square of half is ${square}, area of square is ${area} and area is ${percent}% out of square.`)
}

takeANumber(1);


