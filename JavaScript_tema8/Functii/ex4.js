function calcCircumference(radius){
    circumference=2*Math.PI*radius;
    console.log(`The circumference is ${circumference}.`)
}

function calcArea(radius){
    area=Math.PI*Math.pow(radius,2);
    console.log(`The area is ${area}.`)
}

calcCircumference(5);
calcArea(5);