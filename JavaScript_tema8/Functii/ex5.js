function celsiusToFahrenheit(){
    celsiusTemperature=30;
    fahrenheitTemp=celsiusTemperature*1.8+32;
    console.log(`${celsiusTemperature}°C is ${fahrenheitTemp}°F.`)
}

function fahrenheitToCelsius(){
    fahrenheitTemperature=90;
    celsiusTemp=(fahrenheitTemperature-32)*(5/9);
    console.log(`${fahrenheitTemperature}°F is ${celsiusTemp}°C.`)
}

celsiusToFahrenheit();
fahrenheitToCelsius();