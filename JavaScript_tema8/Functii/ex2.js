function calculateDogAge(puppyAge,humanAge){
    console.log(`Your doggie is ${puppyAge*7} years old in human years!`)
    console.log(`You would be ${Math.round(humanAge/7)} years old in dog years!`)
}

calculateDogAge(3,21);
calculateDogAge(5,28);
calculateDogAge(10,35);