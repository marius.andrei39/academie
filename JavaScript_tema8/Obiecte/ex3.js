let film = {
    title: 'Rush hour',
    duration: 98,
    stars: ['Jackie Chan', 'Chris Tucker']
};

function printFilm(film) {
    console.log(film.title + ' lasts for ' + film.duration + ' minutes');
    let starsString = 'Stars: ';
    for (let i = 0; i < film.stars.length; i++) {
        starsString += film.stars[i];
        if (i != film.stars.length -1) {
            starsString += ', ';
        }
    }
    console.log(starsString);
}

printFilm(film);