let reteta = {
    titlu: 'Cozonac',
    servings: 1,
    ingrediente: ['zahar', 'faina', 'lapte']
};

console.log(reteta.titlu);
console.log('Servings: ' + reteta.servings);
console.log('Ingrediente:');
for (var i = 0; i < reteta.ingrediente.length; i++) {
    console.log(reteta.ingrediente[i]);
}