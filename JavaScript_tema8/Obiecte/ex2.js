let carti = [
    {title: 'Scufita rosie',
     author: 'Fratii Grimm',
     alreadyRead: false
    },
    {title: 'Harap alb',
    author: 'Ion Creanga',
    alreadyRead: true
    }];
  
  for (let i = 0; i < carti.length; i++) {
     let carte = carti[i];
     let info = carte.title + '" by ' + carte.author;
    if (carte.alreadyRead) {
      console.log('You already read "' + info);
    } else{
      console.log('You still need to read "' + info);
    }
  }