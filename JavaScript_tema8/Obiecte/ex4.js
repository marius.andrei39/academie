function validateCreditCard(cardNumber){
    //Verifica daca sunt 16 caractere
    if(cardNumber.length !== 16){
        return false;
    }

    //Verifica daca cele 16 caractere sunt numere
    for(let i = 0; i < cardNumber.length; i++){
        let currentNumber = cardNumber[i];
        currentNumber = Number.parseInt(currentNumber);
        if(!Number.isInteger(currentNumber)){
          return false;
        }
      }

      //Verifica daca ultimul numar este par
      if(cardNumber[cardNumber.length - 1] % 2 !== 0){
        return false;
      }
}