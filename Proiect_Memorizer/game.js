const deck = document.querySelectorAll(".card"),
  multiBtn = document.querySelector(".multiPlayer"),
  singleBtn = document.querySelector(".singlePlayer"),
  restartBtn = document.querySelector("#restartButton"),
  menu = document.querySelector(".menu"),
  game = document.querySelector(".gameplay"),
  winning = document.querySelector(".win-screen");
(bomb1 = document.querySelector(".bomb1")),
  (bomb2 = document.querySelector(".bomb2")),
  (timeValue = document.getElementById("time")),
  (fourBy4Btn = document.querySelector("#fourbyfour")),
  (threeby4Btn = document.querySelector("#threebyfour")),
  (winMessage = document.querySelector(".winMessage")),
  (bombOffImg = document.querySelector(".bomb-off")),
  (winImgBatman = document.querySelector(".win-img-batman")),
  (winImgFlash = document.querySelector(".win-img-flash")),
  (finalScore = document.querySelector(".moreScore"));
deck.forEach((card) => card.addEventListener("click", flipCard));

shuffleDeck(); // randomizes the board before each game

const displayScore1 = document.querySelector("#score1"),
  displayScore2 = document.querySelector("#score2"),
  displayMoves1 = document.querySelector("#moves1"),
  displayMoves2 = document.querySelector("#moves2");

bomb2.style.opacity = "0"; //is hidden because the 1st player always starts

let score1 = 0, // counter for scores
  score2 = 0,
  moves1 = 0, // counter for moves
  moves2 = 0,
  winningScore = 8, // by default the winning score is half of the total matches
  isFirstCard = false,
  first, // variables for the two cards we will flip
  second,
  isBoardClosed = false,
  multiplayer = true, // tells us when we play single/multiplayer
  seconds = 0, // used for timer
  minutes = 0,
  tickTime = 300, // how much time we choose for the timer
  totalScore = 0, // used to count if we flipped all cards
  jokeCounter = 0, // something secret
  p1Turn = true; // used when we switch players

// function used to shuffle the whole deck of cards
function shuffleDeck() {
  deck.forEach((card) => {
    let randomIndex = Math.floor(Math.random() * 16);
    card.style.order = randomIndex;
  });
}

// function used for starting a in-game timer
function startTimer(duration, display) {
  var timer = duration,
    minutes,
    seconds;
  setInterval(function () {
    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);
    minutes = minutes < 10 ? "0" + minutes : minutes;
    seconds = seconds < 10 ? "0" + seconds : seconds;
    display.innerHTML = minutes + ":" + seconds;
    if (--timer < 0) {
      checkGameOver();
    }
  }, 1000);
}

// used for choosing in-game the 3x4 mode
threeby4Btn.onclick = () => {
  for (let i = 0; i < 4; i++) {
    deck[i].classList.add("hide");
  }
  winningScore = 6;
  restart();
};

// used for choosing in-game the 4x4 mode
fourBy4Btn.onclick = () => {
  for (let i = 0; i < 4; i++) {
    deck[i].classList.remove("hide");
  }
  winningScore = 8;
  restart();
};

// used for choosing in menu the multiplayer option
multiBtn.addEventListener("click", () => {
  game.style.display = "block";
  menu.style.display = "none";
  startTimer(tickTime, timeValue);
});

// used for choosing in menu the singleplayer option
singleBtn.addEventListener("click", () => {
  game.style.display = "block";
  menu.style.display = "none";
  document.querySelector(".second-board").style.display = "none";
  multiplayer = false;
  bomb1.style.opacity = "1";
  startTimer(tickTime, timeValue);
  document.querySelector(".first-board").style.margin = "0 0 0 10%";
});

// restarts the whole in-game board, also adds some easter eggs during the game
function restart() {
  score1 = 0;
  score2 = 0;
  moves1 = 0;
  moves2 = 0;
  jokeCounter++;
  displayScore1.textContent = score1.toString();
  displayScore2.textContent = score2.toString();
  displayMoves1.textContent = moves1.toString();
  displayMoves2.textContent = moves2.toString();
  for (let i = 0; i < deck.length; i++) {
    deck[i].classList.remove("flip");
    deck[i].style.pointerEvents = "auto";
  }
  setTimeout(() => {
    shuffleDeck();
  }, 1000);
  p1Turn = true;
  bomb1.style.opacity = "1";
  bomb2.style.opacity = "0";
  document.querySelector(".joke").style.display = "block";
  console.log(jokeCounter);
  if (jokeCounter === 3) {
    document.querySelector(".joke").innerHTML =
      "I already told you. The bomb will go BOOM!!!";
    jokeCounter = 0;
    console.log(jokeCounter);
  } else {
    document.querySelector(".joke").innerHTML =
      "No, no, no. You can't reset the timer";
  }
  setTimeout(() => {
    document.querySelector(".joke").style.display = "none";
  }, 10000);
  resetBoard();
}

//function used to show the front of the cards during game (back of cards is always shown)
function flipCard() {
  if (isBoardClosed) return;
  if (this === first) return;
  this.classList.add("flip");
  if (!isFirstCard) {
    isFirstCard = true; //first click
    first = this; // 'this' = the element that has fired the event
    return;
  }
  isFirstCard = false; //second click
  second = this;
  if (!multiplayer) {
    moves1++;
    displayMoves1.textContent = moves1.toString();
  }
  // if the second card has been chosen, check if they match
  checkMatch();
}

//function used to check if the two cards that we chose are a match
function checkMatch() {
  if (first.dataset.id == second.dataset.id) {
    // so cards cannot be clicked again
    first.style.pointerEvents = "none";
    second.style.pointerEvents = "none";

    resetBoard();
    // if we choose the multiplayer mode we increment the moves and score for both players, otherwise just for the first player
    if (multiplayer) {
      if (p1Turn) {
        score1 += 2;
        displayScore1.textContent = score1.toString();
        moves1++;
        displayMoves1.textContent = moves1.toString();
      } else {
        score2 += 2;
        displayScore2.textContent = score2.toString();
        moves2++;
        displayMoves2.textContent = moves2.toString();
      }
    } else {
      score1 += 2;
      displayScore1.textContent = score1.toString();
    }
    checkGameOver();
  } else {
    // if the cards are not a match then flip them over again
    isBoardClosed = true;
    setTimeout(() => {
      first.classList.remove("flip");
      second.classList.remove("flip");
      isBoardClosed = false;
      resetBoard();
    }, 1000);
    // if multiplayer was chosen then we switch turns each time we don't find a match
    if (multiplayer) {
      if (p1Turn) {
        p1Turn = false;
        displayScore1.textContent = score1.toString();
        moves1++;
        displayMoves1.textContent = moves1.toString();
        setTimeout(() => {
          bomb2.style.opacity = "1";
          bomb1.style.opacity = "0";
        }, 1000);
      } else if (!p1Turn) {
        p1Turn = true;
        displayScore2.textContent = score2.toString();
        moves2++;
        displayMoves2.textContent = moves2.toString();
        setTimeout(() => {
          bomb1.style.opacity = "1";
          bomb2.style.opacity = "0";
        }, 1000);
      }
    }
  }
  totalScore = score1 + score2;
  checkGameOver();
}

// function to 'forget' what cards we've flipped
function resetBoard() {
  first = null;
  second = null;
  isFirstCard = false;
  isBoardClosed = false;
}

// function used to check the winning conditions and show the final page
function checkGameOver() {
  // game is over if either player gets winningScore points and there are no more cards to flip
  if (multiplayer) {
    if (
      score1 >= winningScore &&
      score1 > score2 &&
      totalScore === winningScore * 2
    ) {
      menu.style.display = "none";
      game.style.display = "none";
      winning.style.display = "flex";
      winImgFlash.style.display = "none";
      winImgBatman.style.display = "block";
      finalScore.innerHTML =
        "Batman was able to defeat more villains than Flash";
    } else if (
      score2 >= winningScore &&
      score2 > score1 &&
      totalScore === winningScore * 2
    ) {
      menu.style.display = "none";
      game.style.display = "none";
      winning.style.display = "flex";
      winImgBatman.style.display = "none";
      winImgFlash.style.display = "block";
      finalScore.innerHTML =
        "Flash was able to defeat more villains than Batman";
    }
  } else if (score1 === winningScore * 2) {
    menu.style.display = "none";
    game.style.display = "none";
    winning.style.display = "flex";
    winImgBatman.style.display = "block";
  }

  // if the timer goes to 0 we get a special 'win' page, for Joker
  if (timeValue.innerHTML === "00:00") {
    menu.style.display = "none";
    game.style.display = "none";
    winning.style.display = "flex";
    bombOffImg.style.display = "block";
    winImgBatman.style.display = "none";
    winImgFlash.style.display = "none";
    winMessage.innerHTML = "The bomb went off";
    finalScore.innerHTML = "You were not able to save Robin.";
  }
}
